package io.github.winx64.cmdcool.adapter.versions;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.craftbukkit.v1_8_R2.CraftServer;

import io.github.winx64.cmdcool.adapter.VersionAdapter;

public final class VersionAdapter_1_8_R2 extends VersionAdapter {

	@Override
	public CommandMap getHandle() {
		return ((CraftServer) Bukkit.getServer()).getCommandMap();
	}
}
