package io.github.winx64.cmdcool.configuration;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import io.github.winx64.cmdcool.CommandCooldown;

public abstract class BaseConfiguration {

	protected static final Charset UTF_8 = Charset.forName("UTF-8");

	protected final CommandCooldown plugin;
	protected final String prefix;

	protected final String configFileName;
	protected final File configFile;
	protected final String configVersionKey;
	protected final int configVersion;

	protected FileConfiguration config;
	protected FileConfiguration defaultConfig;

	protected BaseConfiguration(CommandCooldown plugin, String prefix, String configFileName, String configVersionKey,
			int configVersion) {
		this.plugin = plugin;
		this.prefix = prefix;

		this.configFileName = configFileName;
		this.configFile = new File(plugin.getDataFolder(), configFileName);
		this.configVersionKey = configVersionKey;
		this.configVersion = configVersion;
	}

	public final boolean initializeConfiguration() {
		try {
			loadDefaultFileConfiguration();
			loadFileConfiguration();

			prepareConfiguration();

			plugin.log(Level.INFO, "[%s] Configuration loaded successfully!", prefix);
			return true;
		} catch (Exception e) {
			plugin.log(Level.SEVERE, e, "[%s] An error occurred while trying to load the configuration! Details below:",
					prefix);
			return false;
		}
	}

	public final int getConfigVersion() {
		return configVersion;
	}

	private final void loadFileConfiguration() throws Exception {
		if (!configFile.exists()) {
			plugin.log(Level.INFO, "[%s] Configuration file doesn't exist! Creating a new one...", prefix);
			plugin.saveResource(configFileName, true);
		}

		this.config = YamlConfiguration.loadConfiguration(configFile);
		int currentVersion = config.getInt(configVersionKey, -1);
		if (currentVersion != configVersion) {
			String newFileName = configFileName + "." + System.currentTimeMillis() + ".old";
			File newFile = new File(plugin.getDataFolder(), newFileName);
			configFile.renameTo(newFile);
			plugin.log(Level.INFO, "[%s] The old \"%s\" is now \"%s\"", prefix, configFileName, newFileName);
			this.config = YamlConfiguration.loadConfiguration(configFile);
		}
	}

	private final void loadDefaultFileConfiguration() throws Exception {
		try (InputStream input = plugin.getResource(configFileName)) {
			if (input == null) {
				throw new ConfigurationException(
						"The default configuration file doesn't exist inside the plugin's jar file!");
			}
			this.defaultConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(input, UTF_8));
			int defaultConfigVersion = defaultConfig.getInt(configVersionKey, -1);
			if (defaultConfigVersion != configVersion) {
				throw new ConfigurationException(
						"Version mismatch between the current version(%d) and default configuration's version(%d)!",
						configVersion, defaultConfigVersion);
			}
		}
	}

	protected abstract void prepareConfiguration() throws Exception;
}
