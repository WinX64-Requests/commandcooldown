package io.github.winx64.cmdcool.configuration;

import io.github.winx64.cmdcool.CommandCooldown;

public final class CooldownConfiguration extends BaseConfiguration {

	private static final String PREFIX = "Config";
	private static final String CONFIG_FILE_NAME = "config.yml";
	private static final String CONFIG_VERSION_KEY = "config-version";
	private static final int CONFIG_VERSION = 1;

	public CooldownConfiguration(CommandCooldown plugin) {
		super(plugin, PREFIX, CONFIG_FILE_NAME, CONFIG_VERSION_KEY, CONFIG_VERSION);
	}

	@Override
	protected void prepareConfiguration() throws Exception {

	}
}
