package io.github.winx64.cmdcool.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import io.github.winx64.cmdcool.CommandCooldown;
import io.github.winx64.cmdcool.player.CooldownPlayer;

public final class PlayerInOutListener implements Listener {

	private final CommandCooldown plugin;

	public PlayerInOutListener(CommandCooldown plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		plugin.registerCooldownPlayer(new CooldownPlayer(event.getPlayer()));
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		plugin.unregisterCooldownPlayer(event.getPlayer().getUniqueId());
	}
}
