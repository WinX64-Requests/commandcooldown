package io.github.winx64.cmdcool.configuration;

public final class ConfigurationException extends RuntimeException {

	private static final long serialVersionUID = -793672320756754240L;

	public ConfigurationException() {
		super();
	}

	public ConfigurationException(String message) {
		super(message);
	}

	public ConfigurationException(String format, Object... args) {
		super(String.format(format, args));
	}

	public ConfigurationException(Throwable cause, String message) {
		super(message, cause);
	}

	public ConfigurationException(Throwable cause, String format, Object... args) {
		super(String.format(format, args), cause);
	}

	public ConfigurationException(Throwable cause) {
		super(cause);
	}
}
