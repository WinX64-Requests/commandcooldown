package io.github.winx64.cmdcool.configuration;

import java.util.EnumMap;
import java.util.Map;
import java.util.logging.Level;

import org.bukkit.ChatColor;

import io.github.winx64.cmdcool.CommandCooldown;

public final class CooldownMessages extends BaseConfiguration {

	private static final String PREFIX = "Messages";
	private static final String CONFIG_FILE_NAME = "messages.yml";
	private static final String CONFIG_VERSION_KEY = "messages-version";
	private static final int CONFIG_VERSION = 1;

	private Map<Message, String> loadedMessages;

	private boolean warnMissingMessages;
	private boolean warnMissingParameters;

	public CooldownMessages(CommandCooldown plugin, boolean warnMissingMessages, boolean warnMissingParameters) {
		super(plugin, PREFIX, CONFIG_FILE_NAME, CONFIG_VERSION_KEY, CONFIG_VERSION);

		this.loadedMessages = new EnumMap<>(Message.class);

		this.warnMissingMessages = warnMissingMessages;
		this.warnMissingParameters = warnMissingParameters;
	}

	public String get(Message messageType) {
		return loadedMessages.get(messageType);
	}

	public String get(Message messageType, String... arguments) {
		String message = loadedMessages.get(messageType);
		for (int i = 0; i < Math.min(messageType.parameters.length, arguments.length); i++) {
			message = message.replace(messageType.parameters[i], arguments[i]);
		}
		return message;
	}

	public static enum Message {

		// TODO Register messages
		;

		private final String path;
		private final String[] parameters;

		private Message(String path, String... parameters) {
			this.path = path;
			for (int i = 0; i < parameters.length; i++) {
				parameters[i] = "{" + parameters[i] + "}";
			}
			this.parameters = parameters;
		}

		public String getPath() {
			return path;
		}
	}

	@Override
	protected void prepareConfiguration() throws Exception {
		for (Message messageType : Message.values()) {
			String path = messageType.getPath();
			if (!this.config.contains(path) && warnMissingMessages) {
				plugin.log(Level.WARNING, "Missing message \"%s\". Using default value!", path);

				if (!this.defaultConfig.contains(path)) {
					throw new ConfigurationException(
							"Missing message \"%s\" from the default messages. Unable to continue!", path);
				}
				this.loadedMessages.put(messageType,
						ChatColor.translateAlternateColorCodes('&', defaultConfig.getString(path)));
				continue;
			}
			String message = config.getString(path);
			for (String parameter : messageType.parameters) {
				if (!message.contains(parameter) && warnMissingParameters) {
					plugin.log(Level.WARNING, "Missing parameter \"%s\" for message \"%s\"!", parameter, path);
				}
			}
			this.loadedMessages.put(messageType, ChatColor.translateAlternateColorCodes('&', config.getString(path)));
		}
	}
}
