package io.github.winx64.cmdcool;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import io.github.winx64.cmdcool.adapter.VersionAdapter;
import io.github.winx64.cmdcool.configuration.CooldownConfiguration;
import io.github.winx64.cmdcool.configuration.CooldownMessages;
import io.github.winx64.cmdcool.handler.VersionHandler;
import io.github.winx64.cmdcool.listeners.PlayerInOutListener;
import io.github.winx64.cmdcool.player.CooldownPlayer;

/**
 * CommandCooldown's main class
 * 
 * @author WinX64
 *
 */
public final class CommandCooldown extends JavaPlugin {

	private final Map<UUID, CooldownPlayer> cooldownPlayers;

	private final Logger logger;
	private final CooldownConfiguration cooldownConfig;
	private final CooldownMessages cooldownMessages;

	private VersionAdapter versionAdapter;

	public CommandCooldown() {
		this.logger = getLogger();
		this.cooldownPlayers = new HashMap<>();
		this.cooldownConfig = new CooldownConfiguration(this);
		this.cooldownMessages = new CooldownMessages(this, true, true);
	}

	@Override
	public void onEnable() {
		String currentVersion = VersionHandler.getVersion();
		this.versionAdapter = VersionHandler.getAdapter(currentVersion);
		if (versionAdapter == null) {
			logger.severe("The current server version is not supported!");
			if (currentVersion == null) {
				logger.severe(
						"Your current version is unknown. It may be due to a severely outdated server (pre 1.4).");
			} else if (VersionHandler.isVersionUnsupported(currentVersion)) {
				logger.severe("Your current version is " + currentVersion
						+ ". Get off your dinosaur and update your server!");
			} else {
				logger.severe("Your current version is " + currentVersion
						+ ". This is a newer version that still not supported. Ask the author to provide support for it!");
			}
			getServer().getPluginManager().disablePlugin(this);
			return;
		} else {
			logger.info("Registered version adapter with success! Using " + versionAdapter.getClass().getSimpleName());
		}

		if (!cooldownConfig.initializeConfiguration()) {
			logger.severe("Failed to load the configuration. The plugin will be disabled to avoid further damage!");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		if (!cooldownMessages.initializeConfiguration()) {
			logger.severe("Failed to load the messages. The plugin is unable to function correctly without them!");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		for (Player player : Bukkit.getOnlinePlayers()) {
			cooldownPlayers.put(player.getUniqueId(), new CooldownPlayer(player));
		}

		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new PlayerInOutListener(this), this);
	}

	@Override
	public void onDisable() {
		if (versionAdapter != null) {
			this.versionAdapter.unregisterAllHandles();
		}
	}

	public CooldownConfiguration getSignConfig() {
		return cooldownConfig;
	}

	public CooldownMessages getSignMessages() {
		return cooldownMessages;
	}

	public void log(Level level, String format, Object... objects) {
		log(level, null, format, objects);
	}

	public void log(Level level, Exception e, String format, Object... objects) {
		logger.log(level, String.format(format, objects), e);
	}

	public void registerCooldownPlayer(CooldownPlayer cPlayer) {
		cooldownPlayers.put(cPlayer.getUniqueId(), cPlayer);
	}

	public CooldownPlayer getCooldownPlayer(UUID uniqueId) {
		return cooldownPlayers.get(uniqueId);
	}

	public void unregisterCooldownPlayer(UUID uniqueId) {
		cooldownPlayers.remove(uniqueId);
	}

	public VersionAdapter getVersionAdapter() {
		return versionAdapter;
	}
}
