package io.github.winx64.cmdcool.player;

import java.util.UUID;

import org.bukkit.entity.Player;

/**
 * Data class to store the player information related to CommandCooldown
 * 
 * @author WinX64
 *
 */
public final class CooldownPlayer {

	private final Player player;

	public CooldownPlayer(Player player) {
		this.player = player;
	}

	/**
	 * Gets the Player linked to this SmartPlayer
	 * 
	 * @return The Player instance
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Gets the unique id of this player
	 * 
	 * @return The UUID
	 */
	public UUID getUniqueId() {
		return player.getUniqueId();
	}

	/**
	 * Gets the name of this player
	 * 
	 * @return The name
	 */
	public String getName() {
		return player.getName();
	}
}
