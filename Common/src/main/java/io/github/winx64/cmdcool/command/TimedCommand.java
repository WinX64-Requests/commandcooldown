package io.github.winx64.cmdcool.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import io.github.winx64.cmdcool.CooldownEntry;

public final class TimedCommand extends Command {

	private final Command handle;
	private final CooldownEntry entry;

	public TimedCommand(Command handle, CooldownEntry entry) {
		super(handle.getName(), handle.getDescription(), handle.getUsage(), handle.getAliases());

		if (handle instanceof TimedCommand) {
			throw new IllegalArgumentException("The handle of a TimedCommand cannot be another TimedCommand!");
		}

		this.handle = handle;
		this.entry = entry;
	}

	public Command getHandle() {
		return handle;
	}

	public CooldownEntry getEntry() {
		return entry;
	}

	@Override
	public boolean execute(CommandSender sender, String alias, String[] args) {
		return false;
	}
}
