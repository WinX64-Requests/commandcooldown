package io.github.winx64.cmdcool.adapter;

import java.lang.reflect.Field;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;

import io.github.winx64.cmdcool.CooldownEntry;
import io.github.winx64.cmdcool.command.TimedCommand;
import io.github.winx64.cmdcool.reflection.Reflection;

public abstract class VersionAdapter {

	protected static final Field KNOWN_COMMANDS_FIELD = Reflection.getField(SimpleCommandMap.class, "knownCommands");

	protected Map<String, Command> knownCommands;

	public final PluginCommand getPluginCommand(String name) {
		PluginCommand command = (PluginCommand) getHandle().getCommand(name);
		return command instanceof PluginCommand ? (PluginCommand) command : null;
	}

	public final Command getCommand(String name) {
		return getHandle().getCommand(name);
	}

	public final void registerHandle(CooldownEntry entry) {
		Command handle = getCommand(entry.getCommand());
		if (handle == null) {
			return;
		}

		if (handle instanceof TimedCommand) {
			handle = ((TimedCommand) handle).getHandle();
		}

		Command oldHandle = handle;
		TimedCommand newCommand = new TimedCommand(handle, entry);
		getMappedCommands().replaceAll((key, value) -> value == oldHandle ? newCommand : value);
	}

	public final void unregisterAllHandles() {
		getMappedCommands()
				.replaceAll((key, value) -> value instanceof TimedCommand ? ((TimedCommand) value).getHandle() : value);
	}

	protected final Map<String, Command> getMappedCommands() {
		if (knownCommands == null) {
			this.knownCommands = Reflection.getValue(KNOWN_COMMANDS_FIELD, getHandle());
		}
		return knownCommands;
	}

	public abstract CommandMap getHandle();
}
