package io.github.winx64.cmdcool;

import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

@SerializableAs("CooldownEntry")
public final class CooldownEntry implements ConfigurationSerializable {

	private static final String COMMAND_KEY = "command";
	private static final String COOLDOWN_KEY = "cooldown";

	private final String command;
	private final long cooldown;

	public CooldownEntry(String command, long cooldown) {
		this.command = command;
		this.cooldown = cooldown;
	}

	public CooldownEntry(Map<String, Object> map) {
		Object commandObject = map.get(COMMAND_KEY);
		if (commandObject == null) {
			throw new IllegalArgumentException(COMMAND_KEY + " cannot be null!");
		}

		Object cooldownObject = map.get(COOLDOWN_KEY);
		if (cooldownObject == null || !(cooldownObject instanceof Number)) {
			throw new IllegalArgumentException(COOLDOWN_KEY + " cannot be null and must be a number!");
		}

		this.command = commandObject.toString();
		this.cooldown = ((Number) cooldownObject).longValue();
	}

	public String getCommand() {
		return command;
	}

	public long getCooldown() {
		return cooldown;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> map = new LinkedHashMap<>();

		map.put(COMMAND_KEY, command);
		map.put(COOLDOWN_KEY, cooldown);

		return map;
	}

	public static CooldownEntry deserialize(Map<String, Object> map) {
		return new CooldownEntry(map);
	}
}
